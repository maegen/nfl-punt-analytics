# NFL Punt Analytics

## Setup

This repository runs Postgres and Python using Docker. The condensed version of setup commands is
0. `docker-compose up -d`
0. `docker build -t miniconda .`
0. `docker run -it miniconda`


### Postgres
To setup your project on a Mac:
0. Install [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
0. Build, create, and start all images in the background using `docker-compose up -d`. Run `docker logs -f my_postgres`
to be sure it's working and use `docker exec -it my_postgres psql -U postgres` to run psql. Use CTRL-D to exit. (Refer
to the cheat sheet in references for more Docker commands.)
0. Create the database by running `docker exec -it my_postgres psql -U postgres -c "create database nfl_punt_analytics"`
0. To connect to the database through a Postgres GUI such as pgAdmin4, specify Hostname: 127.0.0.1, Port 54320, and
Username postgres. (Double-check that specified port matches values that `docker container ls` returns.)

#### References
0. Setting up Docker with 
[Postgres](https://www.saltycrane.com/blog/2019/01/how-run-postgresql-docker-mac-local-development/)
0. Docker [cheatsheet](https://www.saltycrane.com/blog/2017/08/docker-cheat-sheet/)

### Miniconda
0. Run `docker build --pull -t miniconda .` This will pull the latest image for miniconda3 and install the requirements 
contained within the `environment.yml` file. (If you need to rebuild, add the `--no-cache` option.)
0. `docker run -it -v /path/to/your/repo:/directory/on/docker miniconda` will activate the conda environment
and mount the `/path/to/your/repo` to the `/directory/on/docker` directory. Files created on the container will be
visible locally.

#### References
0. Adding information for miniconda to 
[Dockerfile](https://medium.com/@chadlagore/conda-environments-with-docker-82cdc9d25754)

### Maintenance
0. Run `docker container ls -a --filter status=exited --filter status=created` to see stopped containers.
0. Run `docker container prune` to remove stopped containers.